package edu.phuentelmann.danger;

import edu.phuentelmann.danger.format.FormatCollection;
import edu.phuentelmann.danger.format.FormatCompositeBuilder;

public class FormatCollectionBuilder extends FormatCompositeBuilder {
    public FormatCollectionBuilder() {
        super(FormatCollection::new);
    }
}
