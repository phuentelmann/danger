package edu.phuentelmann.danger.format;

import java.util.List;

public class FormatCollection extends FormatComposite {
    public FormatCollection(List<FormatBase> composites) {
        super(composites);
    }

    public void add(FormatBase item) {
        getComposites().add(item);
    }
}
