package edu.phuentelmann.danger.format;

public abstract class FormatBuilder {
    protected abstract FormatBase buildItem();
}
